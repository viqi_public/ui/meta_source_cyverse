# README #

## Repo of External Metadata Fetcher

This is a simple plugin to enable viewing metadata and annotations fetched from an external server

Note fetched shapes are marked read-only to the renderer and cannot be modified or deleted.

## Quick Setup ###

  1.  Install git
  2.  Install python
  3.  Clone latest source
      ```
        git clone https://gitlab.com/viqi_public/ui/viewer.metadatafetch.git
      ```
  4.  Copy the plugin to your bisque instance
      ```
         cp viewer.metadatafetch/viewer.metadatafetch pathToBisque/bqcore/bq/core/public/plugins
      ```

  5.  Change into the pythonserver directory in the newly checked out directory
      ```
         cd viewer.metadatafetch/pythonserver
         python simpleHttpServer.py
      ```
      Once executed, this window will start up a simple http server which will serve the sample gobs.xml file 

  6.  Open another terminal go to your current bisque install and activate the python environment.
      Stop the server and deploy the public folder.  Restart the server.
      ```
         cd bisque
         workon bisque
         bq-admin server stop
         bq-admin deploy public
         bq-admin server start
      ```
  7.  Access to viqi at http://localhost:8080, navigate to a resource and the sample data should be visible in the resource.