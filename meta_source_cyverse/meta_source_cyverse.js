/*******************************************************************************
  Plugin to fetch additional resource metadata from CyVerse NCI database
  Author: John Delaney, Dima Fedorov

*******************************************************************************/

Ext.namespace('VQ.document_sources');
VQ.document_sources.available = VQ.document_sources.available || [];

Ext.define('VQ.document_sources.CyVerseTCIA', {
    extend: 'VQ.document_sources.Base',
    singleton: true,

    source_url: 'https://tcia.cyverse.org/BisQue/annotations',
    source_name: 'Cyverse TCIA',

    fetch: function(cfg) {
        var u = cfg.url.split('/'),
            uuid = u.pop(),
            url = Ext.String.format('{0}/{1}', this.source_url, uuid);
        this.url = url;
        Ext.Ajax.request({
            url: url,
            callback: function (opts, succsess, response) {
                if (response.status == 404) {
                    this.onLoaded(null);
                } else if (response.status >= 400 || !succsess) {
                    this.onError('Could not fetch the resource from ' + url);
                } else {
                    this.onLoaded(response.responseXml || response.responseText);
                }
            },
            scope: this,
            disableCaching: false,
            timeout: 200000,
        });
    },

    onLoaded: function(doc) {
        try {
            this.resource = BQFactory.parseBQDocument(doc);
        } catch (err) {
            this.resource = null;
        }
        this.fireEvent('loaded', this);
    },

    onError: function(message) {
        //this.error = message;
        console.log('Error while fetching external metadata: ' + message);
        this.resource = null;
        this.fireEvent('loaded', this);
    },

});
VQ.document_sources.available.push(VQ.document_sources.CyVerseTCIA);
